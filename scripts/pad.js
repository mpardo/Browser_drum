function Pad(x, y, colorReleased, colorPressed, keyToBePressed, sample){
  this.x = x;
  this.y = y;
  this.keyP = keyToBePressed;
  this.colorR = colorReleased;
  this.colorP = colorPressed;
  this.play = false;
  this.sample = sample;
  this.afficher = function(){
    if(keysPressed[this.keyP]){
      if(!this.play){
        this.play = true;
        this.sample.play();
      }
      fill(this.colorP);
    }else{
      this.play = false;
      fill(this.colorR);
    }
    rect(this.x, this.y, windowWidth * SIZE_PAD_X, windowHeight * SIZE_PAD_Y);
    fill(0);
    textSize(50);
    text(String.fromCharCode(this.keyP), this.x + ((windowWidth * SIZE_PAD_X) / 2), this.y + ((windowHeight * SIZE_PAD_Y) / 2));
  }
}
