var BACKGROUND_COLOR = 50;

var SIZE_PAD_X = 0.2;
var SIZE_PAD_Y = 0.17;

var POS_PAD_X = new Array();
var POS_PAD_Y = new Array();
var KEYS = new Array();

var DrumsSample = {
  Snare : 0,
  Rim : 1,
  OpHH : 2,
  ClHH : 3,
  Kick : 4,
  Crash : 5,
  Ride : 6,
  HighTom : 7,
  MedTom : 8,
  BassTom : 9
}

var SOUNDS;

function preload(){
  SOUNDS = [];

  SOUNDS.push(loadSound('/Ressources/Samples/s.mp3'));
  SOUNDS.push(loadSound('/Ressources/Samples/rs.mp3'));
  SOUNDS.push(loadSound('/Ressources/Samples/ohh.mp3'));
  SOUNDS.push(loadSound('/Ressources/Samples/chh.mp3'));
  SOUNDS.push(loadSound('/Ressources/Samples/k.mp3'));
  SOUNDS.push(loadSound('/Ressources/Samples/cc.mp3'));
  SOUNDS.push(loadSound('/Ressources/Samples/cr.mp3'));
  SOUNDS.push(loadSound('/Ressources/Samples/t1.mp3'));
  SOUNDS.push(loadSound('/Ressources/Samples/t2.mp3'));
  SOUNDS.push(loadSound('/Ressources/Samples/t3.mp3'));
}

KEYS.push(69);
KEYS.push(66);
KEYS.push(67);
KEYS.push(68);
KEYS.push(65);
KEYS.push(70);
KEYS.push(71);
KEYS.push(72);
KEYS.push(73);
KEYS.push(74);

POS_PAD_X.push(0.2);
POS_PAD_X.push(0.6);
POS_PAD_X.push(0.1);
POS_PAD_X.push(0.4);
POS_PAD_X.push(0.7);
POS_PAD_X.push(0.2);
POS_PAD_X.push(0.6);
POS_PAD_X.push(0.1);
POS_PAD_X.push(0.4);
POS_PAD_X.push(0.7);

POS_PAD_Y.push(0.17);
POS_PAD_Y.push(0.17);
POS_PAD_Y.push(0.33);
POS_PAD_Y.push(0.33);
POS_PAD_Y.push(0.33);
POS_PAD_Y.push(0.50);
POS_PAD_Y.push(0.50);
POS_PAD_Y.push(0.66);
POS_PAD_Y.push(0.66);
POS_PAD_Y.push(0.66);
