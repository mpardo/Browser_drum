var pads;
var keysPressed;

function setup(){
  createCanvas(windowWidth, windowHeight);
  pads = new Array();
  keysPressed = new Array();
  for(var i = 0; i < 256; i++){
    keysPressed.push(false);
  }
  createPads();
}

function draw(){
  background(BACKGROUND_COLOR);
  drawPads(keysPressed);
}

function createPads(){
  for(var i = 0; i < 10; i++){
    pads.push(new Pad(POS_PAD_X[i] * windowWidth, POS_PAD_Y[i] * windowHeight, color(255), color(144), KEYS[i], SOUNDS[i]));
  }
}

function drawPads(){
  for (var i = 0; i < 10; i++) {
    pads[i].afficher();
  }
}

function windowResized(){
  resizeCanvas(windowWidth, windowHeight);
}

function keyPressed(){
  keysPressed[keyCode] = true;
}

function keyReleased(){
  keysPressed[keyCode] = false;
}
